#include <stdio.h>

#define MAX 30

typedef struct edge {
  int u, v, w;
} edge;

typedef struct edge_list {
  edge data[MAX];
  int n;
} edge_list;

edge_list elist;

int Graph[MAX][MAX],n;
edge_list spanlist;

void kruskalAlgo();
int find(int belongs[], int vertexno);
void applyUnion(int belongs[], int c1, int c2);
void sort();
void print();

void kruskalAlgo() {
  int belongs[MAX], i, j, cno1, cno2;
  elist.n = 1;

  for (i = 1; i <= n; i++)
    for (j =1 ; j <=n; j++) {
      if (Graph[i][j] != 0) {
        elist.data[elist.n].u = i;
        elist.data[elist.n].v = j;
        elist.data[elist.n].w = Graph[i][j];
        elist.n++;
      }
    }

  sort();

  for (i = 1; i <=n; i++)
    belongs[i] = i;

  spanlist.n = 1;

  for (i = 1; i <=elist.n; i++) {
    cno1 = find(belongs, elist.data[i].u);
    cno2 = find(belongs, elist.data[i].v);

    if (cno1 != cno2) {
      spanlist.data[spanlist.n] = elist.data[i];
      spanlist.n = spanlist.n + 1;
      applyUnion(belongs, cno1, cno2);
    }
  }
}

int find(int belongs[], int vertexno) {
  return (belongs[vertexno]);
}

void applyUnion(int belongs[], int c1, int c2) {
  int i;

  for (i = 1; i <= n; i++)
    if (belongs[i] == c2)
      belongs[i] = c1;
}

void sort() {
  int i, j;
  edge temp;

  for (i = 2; i <=elist.n; i++)
    for (j = 1; j <=elist.n - 1; j++)
      if (elist.data[j].w > elist.data[j + 1].w) {
        temp = elist.data[j];
        elist.data[j] = elist.data[j + 1];
        elist.data[j + 1] = temp;
      }
}

void print() {
  int i, cost = 0;
  printf("Edge : Weight\n");
  for (i = 1; i <= spanlist.n; i++) {
    printf("\n%d - %d : %d", spanlist.data[i].u, spanlist.data[i].v, spanlist.data[i].w);
    cost = cost + spanlist.data[i].w;
  }

  printf("\nSpanning tree cost: %d", cost);
}

void display() {
    int i, j;
    for (i = 1; i <=n; i++) {
        for (j = 1; j <=n; j++)
            printf("%4d", Graph[i][j]);
        printf("\n");
    }
}

create_graph() {
    int i, max_edges, origin, destin;
    max_edges = n * (n - 1);
      for (i = 1; i <=max_edges; i++) {
        printf("Enter edge %d( 0 0 ) to quit : ", i);
        scanf("%d %d", &origin, &destin)    ;
        if ((origin == 0) && (destin == 0))
            break;
        if (origin > n || destin > n || origin <= 0 || destin <= 0) {
            printf("Invalid edge!\n");
            i--;
        }
        else {
            printf("\n Enter the weight: ");
            scanf("%d",&Graph[origin][destin]);
        }

    }
}

int main() {
    printf("\n Enter the no. of vertices in the graph: ");
    scanf("%d",&n);
    create_graph();
    printf("\n Matrix: \n");
    display();
    kruskalAlgo();
    print();
    return 0;
}
      