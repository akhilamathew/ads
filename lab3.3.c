#include<stdio.h>
#include<conio.h>
#define max 20
int adj[max][max];

#define INF 9999999

void display(int n) {
    int i, j;
    for (i = 1; i <=n; i++) {
        for (j = 1; j <=n; j++)
            printf("%4d", adj[i][j]);
        printf("\n");
    }
}

create_graph(int n) {
    int i, max_edges, origin, destin;
    max_edges = n * (n - 1);
      for (i = 1; i <= max_edges; i++) {
        printf("Enter edge %d( 0 0 ) to quit : ", i);
        scanf("%d %d", &origin, &destin)    ;
        if ((origin == 0) && (destin == 0))
            break;
        if (origin > n || destin > n || origin <= 0 || destin <= 0) {
            printf("Invalid edge!\n");
            i--;
        }
        else {
            printf("\n Enter the weight: ");
            scanf("%d",&adj[origin][destin]);
        }

    }
}

int main() {
    int no_edge,v,x,y,i,j,cost=0;
    printf("\n Enter the no. of vertices in the graph: ");
    scanf("%d",&v);
    create_graph(v);
    printf("\n Matrix: \n");
    display(v);
    int visited[v];
    for(i=1; i<=v; i++)
        visited[i]=0;
    no_edge = 0;
    visited[1] =1;
    printf("Edge : Weight\n");
    while (no_edge < v - 1) {
        int min = INF;
        x = 0;
        y = 0;
        for (i = 1; i <= v; i++) {
            if (visited[i]) {
                for ( j = 1; j <= v; j++) {
                    if (!visited[j] && adj[i][j]) {
                        if (min > adj[i][j]) {
                            min = adj[i][j];
                            x = i;
                            y = j;
           
                        }
                    }
                }
            }
          }
          printf("%d - %d : %d\n",x,y,adj[x][y]);

          cost=cost+adj[x][y];
          visited[y] = 1;
          no_edge++;
       
    }

printf("\n Total cost : %d",cost);

    return 0;
}