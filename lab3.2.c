#include<stdio.h>
#include<stdlib.h>

#define max 100
int adj[max][max],visited[max],n;
int s[max],top=-1;
int transpose[max][max],visited1[max];

void push(int item){
  if(top==n)
    printf("Stack is full");
  else{
    top++;
    s[top]=item;
  }
  
}

int pop() {
  int data;   
  if(top!=-1){ 
    data = s[top]; 
    top = top - 1;
    return data; 
  } 
  else 
  printf("Could not retrieve data, Stack is empty.\n"); 
 }

void display() {
            int i, j;
            printf("\n Adjacency matrix: \n");
            for (i = 1; i <=n; i++) {
                    for (j = 1; j <=n; j++)
                            printf("%4d", adj[i][j]);
                    printf("\n");
               
        }
    }

void create_graph() {
            int i, max_edges, origin, destin;
            max_edges = n * (n - 1);
              for (i = 1; i <=max_edges; i++) {
                    printf("Enter edge %d( 0 0 ) to quit : ", i);
                    scanf("%d %d", &origin, &destin)    ;
                    if ((origin == 0) && (destin == 0))
                            break;
                    if (origin > n || destin > n || origin <0 || destin < 0) {
                            printf("Invalid edge!\n");
                            i--;
                     }
                    else
                            adj[origin][destin]=1;
            }
    }

void reverse() {
  for(int i=1;i<=n;i++){
    for(int j=1;j<=n;j++)
        transpose[j][i]=adj[i][j];
  }
  printf("\n Transpose matrix: \n");
  int i,j;
  for (i = 1; i <=n; i++) {
    for (j = 1; j <=n; j++)
      printf("%4d", transpose[i][j]);
    printf("\n");
  }
  
}  
   
void DFS2(){
  int i=pop();
  visited1[i] = 1;
  printf("%d \t",i);
        for (int j = 1; j <=n; j++)
        {
            if(transpose[i][j]==1 && !visited[j])
                DFS();
            else if(transpose[i][j]!=1 && visited[j]==1)
                continue;
            else
                 printf("%d \t", j);
                
        }
  
  
}

void DFS(int i) {
        visited[i] = 1;
        printf("%d \t",i);
        for (int j = 1; j <=n; j++)
        {
            if(adj[i][j]==1 && !visited[j])
                DFS(j);
            else if(adj[i][j]!=1 && visited[j]==1)
                continue;
            else{
                 push(j);
                 return;
             }
                
        }
        
}

int main() {
        int ch;
        printf("\n Enter the no. of vertices: ");
        scanf("%d",&n);
        create_graph();
        display();
        DFS(1);
        reverse();
        DFS2();
}